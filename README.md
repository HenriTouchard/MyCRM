### Redux data should have
=> registered contats

### Components:
=> one view to see list of contacts;
=> one view to edit a contact;
=> one view to create new contact.

################## Destructuration du programme en composants ##############

  ### ContactList* qui est chargé d'afficher la liste des components Contact
    # Contact qui va contenir un DropdownButton
      # Photo de profil + informations + ActionButtons => delete/edit/history
    # Scrollable Dialog qui s'ouvre avec l' ActionButton "history" présente les actions effectuées pour le suivi du client
    # NewContactButton => permet d'accèder à la page de création d'un contact.
  #

  ### ContactChange* chargé de la création et de l'édition des contacts.
    # Form
  #

  *seront des containers puisqu'ils seront reliés à connect*


############### Questionnement sur les données de state ################

1. Is it passed in from a parent via props? If so, it probably isn’t state.
2. Does it remain unchanged over time? If so, it probably isn’t state.
3. Can you compute it based on any other state or props in your component? If so, it isn’t state.

=> Le state contiendra donc toutes les données utiles au component Contact puisqu'ils ne peuvent pas êtres calculés et changent
   au cour de l'utilisation de l'application.
