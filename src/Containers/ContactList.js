import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import defaultContactPicture from '../res/contact_icon2.png';
import Contact from '../Components/Contact';
import { handleContact } from '../Actions/index';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';


/**
 * This class is a container: it allows to provide all formatted requested data to components it renders.
 */
class ContactList extends Component {
    constructor(props) {
        super(props);
        this.state = this.props.contacts;
    }

    // Call the actions function to remove contact from store
    onRemoveHandler(contactId) {
        this.props.handleContact("CONTACT_REMOVE", contactId);
    }

    // Contact modification is accomplshed by sending informations through URL.
    // This function return formated url by giving contactId.
    getContactEditLink(contactId) {
        var link = JSON.stringify(this.state[contactId])
        if (link === undefined) {
            return "#";
        }
        link = "/new/:contact:" + link;
        return link;
    }

    // This funtion opens a Dialog to enter a status text which will appear in contact informations    
    onAddStatus(status, contactId) {
        var data = [status, contactId];
        this.props.handleContact("ADD_STATUS", data);
    }

    renderList() {
        // Check if there are contacts
        if (_.isEmpty(this.props.contacts)) {
            console.log(this.props.contacts);
            return <span className="no-contact">vous n'avez pas encore de contacts</span>
        }
        
        // For each contact render this and call the function selectContact if selected
        return (
            // Loop on each contact provided by the store (in props) to extract respectives informations
            // by extracting contactId. It returns a list of formated contacts rows
            Object.keys(this.props.contacts).map((key, index) => {
                let contact = this.props.contacts[key];
                if (contact.url === undefined)
                    contact.url = defaultContactPicture;
                if (contact.status === undefined)
                    contact.status = "none";
                let editLink = this.getContactEditLink(key)

                return (
                    <li key={key}>
                        <Contact
                            contact={contact}
                            liKey={key}
                            editLink={editLink}
                            addStatus={this.onAddStatus.bind(this)}
                            removeContact={this.onRemoveHandler.bind(this, key)}
                        />
                    </li>
                );
            })
        );
    }

    // Render of course, i should maybe put the button in a different Component
    render() {
        return (
            <div>
                <Link className="btn btn-primary AddContactBtn" to="/new/:contact:null">
                    <strong>+</strong>
                </Link>
                <ul className="list-group col-sm-12">
                    {this.renderList()}
                </ul>
            </div>
        );
    }
}

// allow to store contacts from the redux store in the props of the above class.
function mapStateToProps(state) {
    return {
        contacts: state.contacts
    };
}

// allow to export actions in props of the class to call it from method (onRemove && addStatus)
function mapToDispatchToProps(dispatch) {
    return bindActionCreators({ handleContact: handleContact }, dispatch);
}

// connect from redux give link to the store to functions mapToDispatchToProps and mapStateToProps
export default connect(mapStateToProps, mapToDispatchToProps)(ContactList);
