import React from 'react';
import { createStore } from 'redux';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import reducers from './Reducers';
// import devToolsEnhancer from 'remote-redux-devtools';
import { loadState, saveState } from './Localstorage';

import ContactNew from './Components/ContactNew';
import ContactList from './Containers/ContactList';

import Burger from './Components/Menu';
import './index.css';


import registerServiceWorker from './registerServiceWorker';

const initialState = loadState();

// Initialize the store to contain all data.
function configureStore(initialState) {
    const store = (window.devToolsExtension ? window.devToolsExtension()(createStore) : createStore)(reducers, initialState);
    // Enable Webpack hot module replacement for reducers 
    if (module.hot) {
        module.hot.accept('./Reducers', () => {
            const nextReducer = require('./Reducers');
            store.replaceReducer(nextReducer);
        });
    }
    return store;
}

const store = configureStore(initialState);
store.subscribe(() => {
    saveState(store.getState());
});

const links = ["/", "/new/:contact:null"];
const titles = ["Mes contacts", "Nouveau contact"];

// Contains the App and handle routes to change pages.
ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <div>
                <Burger links={links} titles={titles} />
                <Switch>
                    <Route path="/new/:contact" component={ContactNew} />
                    <Route path="/" component={ContactList} />
                </Switch>
            </div>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
