import { combineReducers } from 'redux';
import { reducer as formReducer} from 'redux-form';
import ReducerNewContact from "./Create_reducer";


const rootReducer = combineReducers({
    contacts: ReducerNewContact,
    form: formReducer,
});

export default rootReducer;
