
export default function (state = {}, action) {

    switch (action.type) {
        //check if Contact exists and store new or dedited element in store
        case 'CONTACT_CREATE':
            const id = action.payload.surname;
            // is true if we are editting a contact and false if it's created
            if (state[id]) {
                delete (state[id]);
            }
            return Object.assign({}, state, { [id]: action.payload });

        //delete and send new state by copy in store.
        case 'CONTACT_REMOVE':
            delete (state[action.payload]);
            return Object.assign({}, state);

        // Insert new status in contact.id add New status in store.
        case 'ADD_STATUS':
            // action payload contains [status, contactId]
            const newContact = state[action.payload[1]]; 
            if (newContact === undefined){
                return state;
            }
            newContact.status = action.payload[0];
            delete (state[action.payload[1]]);
            return Object.assign({}, state, { [action.payload[1]]: newContact });
        default:
            return state;
    }
}

//il semble qu'il ne faille pas toucher au state directement mais je n'ai pas trouvé de réponse claire sur le pourquoi.