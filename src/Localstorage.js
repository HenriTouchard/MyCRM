// save store in local storage
export const saveState = (state) => {
    try{
        const serializedState = JSON.stringify(state);
        localStorage.setItem('crmState', serializedState);
    }catch (err) {
        console.log("writing in cache failed because of : ", err);
    }
};

// Load state from local storage (only at initialisation of the store)
export const loadState = () => {
    try{
        const serializedState = localStorage.getItem('crmState');
        if (serializedState === null)
            return undefined;
        return  JSON.parse(serializedState);
    }catch (err){
        return undefined;
    }
};