
/**
 * each function is called by components to access
 * store via redux. It returns an action type to specify what to do
 * and data.
 */
export function createContact(contact, callback) {
    callback()
    return {
        type: 'CONTACT_CREATE',
        payload: contact
    };
}

export function handleContact(action, data) {
    if (action === "CONTACT_REMOVE") {
        return {
            type: 'CONTACT_REMOVE',
            payload: data
        };
    }

    else if (action === "ADD_STATUS") {
        return {
            type: 'ADD_STATUS',
            payload: data
        };
    }
}
