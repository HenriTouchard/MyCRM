import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createContact } from '../Actions';

import defaultContactPicture from '../res/contact_icon.png';

export class ContactNew extends Component {
    constructor(props) {
        super(props);
        var url = decodeURIComponent(this.props.location.pathname);
        if (url !== undefined) {
            url = url.split("/:contact:")[1];
            var contact = JSON.parse(url);
            if (contact !== null) {
                this.props.initialize(contact);
            }
        }
    }

    //allow to treat each field by a generic way and give danger ability
    renderNormalField(field) {
        const { meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;
        var input = "";
        if (field.focus === "autoFocus")
            input = (
                <input
                    autoFocus
                    className="form-control"
                    type="text"
                    value="hello"
                    {...field.input}
                />
            );
        else
            input = (<input
                value="hello"
                className="form-control"
                type="text"
                {...field.input}
            />);

        return (
            <div className={className}>
                <label>{field.label}</label>
                {input}
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    // Send The new contacts data to the store with a callback to redirect user to the list page
    onSubmit(values) {
        this.props.createContact(values, () => { this.props.history.push('/') });
    }

    // Render the form 
    render() {
        const { handleSubmit } = this.props;
        return (
            <div>
                <center><img src={defaultContactPicture} alt="" /></center>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <Field
                        name="surname"
                        label="Nom"
                        component={this.renderNormalField}
                        focus="autoFocus"
                    />
                    <Field
                        name="name"
                        label="Prénom"
                        component={this.renderNormalField}
                    />
                    <Field
                        name="url"
                        label="url de photo de profil"
                        component={this.renderNormalField}
                    />
                    <Field
                        name="phone"
                        label="Téléphone"
                        component={this.renderNormalField}
                    />
                    <Field
                        name="mail"
                        label="Mail"
                        component={this.renderNormalField}
                    />
                    <Field
                        name="adress"
                        label="Adresse"
                        component={this.renderNormalField}
                    />
                    <Field
                        name="company"
                        label="entreprise"
                        component={this.renderNormalField}
                    />
                    <div>
                        <button type="submit" className="btn submitter">Enregistrer</button>
                        <Link to="/" className="btn btn-danger canceler">Annuler</Link>
                    </div>
                </form>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};

    if (!values.surname) {
        errors.surname = "Entrez un nom";
    }
    if (!values.name) {
        errors.name = "Entrez un prénom";
    }
    if (!values.phone) {
        errors.phone = "Entrez un téléphone sous le format suivant 06******98 ";
    }
    return errors;
}

export default reduxForm({
    validate,
    form: 'ContactNewForm'
})(
    connect(null, { createContact })(ContactNew)
    //null= ce que je prends comme state
    );

  //peut servir pour créer les contacts 
