import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';

/**
 * Open a dialog asking the task you did and the date you did it
 */
export default class DialogAction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            status: ""
        };
    }

    // Make the dialog visible when clicking on the button addStatus
    handleOpen = () => {
        this.setState({ open: true });
    };

    //Handle when user is entering text
    handleStatusChange(event) {
        this.setState({ status: event.target.value });
    }

    //make a call to parent's function "onAddAction" given via props and close the dialog. 
    onSubmit() {
        this.props.onChange(this.state.status, this.props.contact);
        this.setState({ open: false });
    }

    render() {
        return (
            <div>
                <RaisedButton style={this.props.style} label="Add status" onClick={this.handleOpen} />
                <Dialog
                    title="Add status"
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    What is your client status?
                    <form>
                        <input type="text" name="status" placeholder="Status" value={this.state.status} onChange={this.handleStatusChange.bind(this)} />
                        <button type="button" onClick={this.onSubmit.bind(this)}>Ok</button>
                    </form>
                </Dialog>
            </div>
        );
    }
}