import React, { Component } from 'react';
import DialogAction from './DialogAction';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Link } from 'react-router-dom';
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card';

/**
 * OPen a dialog asking the task you did and the date you did it
 */
export default class Contact extends Component {
    //make a call to parent's function "onAddAction" given via props and close the dialog. 
    
    getContactInfo(contact) {
        return (
            <div className="col-sm-9">
                <div> Email : {contact.mail}</div><br />
                <div> Téléphone : {contact.phone}</div><br />
                <div> Adresse : {contact.adress}</div><br />
                <div> Status : {contact.status}</div><br />
            </div>
        );
    }

    render() {
        //Définit le style à appliquer pour les éléments de material-ui
        var labelstyles = {
            width: '200px',
            margin: '3px',
            float: 'none'

        }
        //for each call we have a contact given in props
        const contact = this.props.contact;
        //for each call we have the contactId given in props
        const key = this.props.liKey;
        return (
            <MuiThemeProvider>
                <Card>
                    <CardHeader
                        title={contact.name}
                        subtitle={contact.surname}
                        key={contact.surname}
                        avatar={contact.url}
                        actAsExpander={true}
                        showExpandableButton={true}
                    />
                    <CardText id="card-text"
                        expandable={true}>
                        {this.getContactInfo(this.props.contact)}
                        <div className="col-sm-3">
                            <CardActions>
                                <div className="action-btn">
                                    <RaisedButton style={labelstyles} label="Delete Contact" secondary={true} onClick={this.props.removeContact} />
                                </div>
                                <div className="action-btn">
                                    <Link to={this.props.editLink}>
                                        <RaisedButton style={labelstyles} label="Edit contact" />
                                    </Link>
                                </div>
                                <div className="action-btn">
                                    <DialogAction style={labelstyles} onChange={this.props.addStatus} contact={key} />
                                </div>
                            </CardActions>
                        </div>
                    </CardText>
                </Card>
            </MuiThemeProvider>
        );
    }
}

