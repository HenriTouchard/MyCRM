import React, { Component } from 'react';
import { slide as Menu } from 'react-burger-menu';
import { Link } from 'react-router-dom';

/**
 *  this is a simple sliding menu (or menu burger)
 */
export default class Burger extends Component {
  render () {
    return (
        <div className="navbar navbar-fixed-top navbar-default">
                <Menu left >
                  <Link className="menu-item" to={this.props.links[0]}>
                    {this.props.titles[0]}
                  </Link>
                  <Link className="menu-item" to={this.props.links[1]}>
                  {this.props.titles[1]}
                  </Link>
                </Menu>
        </div>
    );
  }
}
